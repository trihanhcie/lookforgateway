﻿namespace LookForGateway.Core.Constants
{
    public class ConfigConstants
    {
        public const string Identity = "Identity";
        public const string LookForIdentityUrl = "LookForIdentityUrl";
        public const string ApiName = "ApiName";
        public const string ApiSecret = "ApiSecret";

    }
}
