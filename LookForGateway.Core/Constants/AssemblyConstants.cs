﻿namespace LookForGateway.Core.Constants
{
  public class AssemblyConstants
  {
    public const string UseCases_Tradesman = "LookForGateway.UseCases.Tradesman";
    public const string UseCases_Jobs = "LookForGateway.UseCases.Jobs";

  }
}
