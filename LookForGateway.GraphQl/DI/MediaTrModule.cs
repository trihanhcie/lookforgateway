﻿using Autofac;
using LookForGateway.Common.Extensions.MediaTR;
using LookForGateway.Core.Constants;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace LookForGateway.GraphQl.DI
{
  public class MediaTrModule : Autofac.Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();
      builder.RegisterGeneric(typeof(ExceptionBehavior<,>)).As(typeof(IPipelineBehavior<,>));
      builder.RegisterGeneric(typeof(LoggingBehavior<,>)).As(typeof(IPipelineBehavior<,>));
      builder.RegisterGeneric(typeof(Logger<>)).As(typeof(ILogger<>)).SingleInstance();


      var mediatrOpenTypes = new[]
     {
                typeof(IRequestHandler<,>),
            };

      foreach (var mediatrOpenType in mediatrOpenTypes)
      {
        builder
            .RegisterAssemblyTypes(
            Assembly.Load(AssemblyConstants.UseCases_Jobs),
            Assembly.Load(AssemblyConstants.UseCases_Tradesman)
            )
            .AsClosedTypesOf(mediatrOpenType)
            .AsImplementedInterfaces();
      }

      builder.Register<ServiceFactory>(ctx =>
      {
        var c = ctx.Resolve<IComponentContext>();
        return t => c.Resolve(t);
      });

    }
  }
}
