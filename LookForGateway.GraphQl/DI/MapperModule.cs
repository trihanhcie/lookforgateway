﻿using Autofac;
using AutoMapper;
using LookForGateway.UseCases.Jobs.Mapper;
using LookForGateway.UseCases.Tradesman.Mapper;

namespace LookForGateway.GraphQl.DI
{
  public class MapperModule : Autofac.Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.Register(c => new MapperConfiguration(cfg =>
      {
        cfg.AddProfile(new TradesmanProfile());
        cfg.AddProfile(new JobProfile());
      })).AsSelf().SingleInstance();

      builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();
    }
  }
}
