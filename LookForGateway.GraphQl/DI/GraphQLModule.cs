﻿using Autofac;
using GraphQL;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel;
using Microsoft.AspNetCore.Http;
using System.Reflection;

namespace LookForGateway.GraphQl.DI
{
  public class GraphQLModule : Autofac.Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterType<DocumentExecuter>().As<IDocumentExecuter>();
      builder.RegisterType<LookForGatewayMutation>().AsSelf();
      builder.RegisterType<LookForGatewayQuery>().AsSelf();
      builder.RegisterType<LookForGatewaySchema>().As<ISchema>().InstancePerRequest();
      builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(x => x.Name.EndsWith("Type")).AsSelf();


      //builder.RegisterType<PrincipalListener>().AsImplementedInterfaces();
      builder.RegisterType<HttpContextAccessor>().AsImplementedInterfaces();
      builder.RegisterType<DocumentWriter>().AsImplementedInterfaces().SingleInstance();
      builder.Register((c, t) =>
      {
        var context = c.Resolve<IComponentContext>();
        var schema = new LookForGatewaySchema(new GraphQL.FuncServiceProvider(type => context.Resolve(type)));
        return schema;
      }).AsImplementedInterfaces();

    }
  }
}
