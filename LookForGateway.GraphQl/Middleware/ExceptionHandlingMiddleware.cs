﻿using Grpc.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace LookForGateway.GraphQl.Middleware
{
  public class ExceptionHandlingMiddleware
  {
    private readonly RequestDelegate _next;

    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
      _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
      try
      {
        await _next(httpContext);
      }
      catch (RpcException ex) when (ex.StatusCode == StatusCode.NotFound)
      {
        
      }
      catch (RpcException ex) when (ex.StatusCode == StatusCode.AlreadyExists)
      {

      }
      catch (RpcException ex) when (ex.StatusCode == StatusCode.InvalidArgument)
      {

      }
      catch (RpcException ex)
      {

      }
    }
  }

  public static class ExceptionHandlingMiddlewareExtensions
  {
    public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
    {
      return builder.UseMiddleware<ExceptionHandlingMiddleware>();
    }
  }
}
