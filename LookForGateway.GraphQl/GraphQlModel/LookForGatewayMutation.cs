﻿using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.GraphQlModel.Mutation;

namespace LookForGateway.GraphQl.GraphQlModel
{
  public class LookForGatewayMutation : ObjectGraphType
  {
    public LookForGatewayMutation()
    {
      Name = "LookForGatewayMutation";
      Field<TradesmanMutationGraphType>(GraphQLArgumentConstant.TradesmanMutationGroup, resolve: context => new { });
      Field<JobMutationGraphType>(GraphQLArgumentConstant.JobMutationGroup, resolve: context => new { });

    }
  }
}
