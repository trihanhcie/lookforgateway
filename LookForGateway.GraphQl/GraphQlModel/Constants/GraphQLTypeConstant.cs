﻿namespace LookForGateway.GraphQl.GraphQlModel.Constants
{
  public class GraphQLTypeConstant
  {
    public class Tradesman
    {
      public const string GetByEmail = "getEmail";
      public const string UpdateByEmail = "updateByEmail";
    }

    public class Job
    {
      public const string AddLeadByEmail = "addLeadByEmail";
      public const string GetByCode = "getByCode";

    }
  }
}
