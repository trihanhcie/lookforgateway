﻿namespace LookForGateway.GraphQl.GraphQlModel.Constants
{
  public class GraphQLArgumentConstant
  {
    public const string Input = "input";
    public const string Email = "email";
    public const string TradesmanQueryGroup = "tradesman_query";
    public const string TradesmanMutationGroup = "tradesman_mutation";
    public const string JobQueryGroup = "job_query";
    public const string JobMutationGroup = "job_mutation";

    public class Tradesman
    {
      public const string FirstName = "firstName";
      public const string LastName = "lastName";
      public const string FullAddress = "fullAddress";
    }

    public class Job
    {
      public const string Code = "code";
      public const string TradesmanId = "tradesmanId";
      public const string Description = "description";
      public const string FullAddress = "fullAddress";
    }
  }
}