﻿using GraphQL;
using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.GraphQlModel.Types.Query;
using LookForGateway.UseCases.Jobs.Queries;
using MediatR;

namespace LookForGateway.GraphQl.GraphQlModel.Query
{
  public class JobQueryGraphType : ObjectGraphType
  {
    public JobQueryGraphType(IMediator mediaTr)
    {
      Name = GraphQLArgumentConstant.JobQueryGroup;
      FieldAsync<GetJobByCodeQueryResponseType>(GraphQLTypeConstant.Job.GetByCode,
                 arguments: new QueryArguments(new QueryArgument<GetJobByCodeQueryType> { Name = GraphQLArgumentConstant.Input }),
                 resolve: async context =>
                 {
                   var query = context.GetArgument<GetJobByCodeQuery>(GraphQLArgumentConstant.Input);
                   return await mediaTr.Send(query);
                 });
    }
  }
}
