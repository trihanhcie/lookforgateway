﻿using GraphQL;
using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.GraphQlModel.Types.Query;
using LookForGateway.UseCases.Tradesman.Queries;
using MediatR;

namespace LookForGateway.GraphQl.GraphQlModel.Query
{
  public class TradesmanQueryGraphType : ObjectGraphType
  {
    public TradesmanQueryGraphType(IMediator mediaTr)
    {
      Name = GraphQLArgumentConstant.TradesmanQueryGroup;
      FieldAsync<GetTradesmanByEmailQueryType>(GraphQLTypeConstant.Tradesman.GetByEmail,
                 arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = GraphQLArgumentConstant.Input }),
                 resolve: async context =>
                 {
                   var query = context.GetArgument<GetTradesmanByEmailQuery>(GraphQLArgumentConstant.Input);
                   return await mediaTr.Send(query);
                 });
    }
  }
}
