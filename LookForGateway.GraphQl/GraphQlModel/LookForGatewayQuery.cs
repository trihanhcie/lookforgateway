﻿using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.GraphQlModel.Query;

namespace LookForGateway.GraphQl.GraphQlModel
{
  public class LookForGatewayQuery : ObjectGraphType
  {
    public LookForGatewayQuery()
    {
      Name = "LookForGatewayQuery";
      Field<TradesmanQueryGraphType>(GraphQLArgumentConstant.TradesmanQueryGroup, resolve: context => new { });
      Field<JobQueryGraphType>(GraphQLArgumentConstant.JobQueryGroup, resolve: context => new { });

    }
  }
}
