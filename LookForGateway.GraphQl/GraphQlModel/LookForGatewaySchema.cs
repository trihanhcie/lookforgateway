﻿using GraphQL.Types;
using GraphQL.Utilities;
using System;

namespace LookForGateway.GraphQl.GraphQlModel
{
  public class LookForGatewaySchema : Schema
  {
    public LookForGatewaySchema(IServiceProvider provider) : base(provider)
    {
      Query = provider.GetRequiredService<LookForGatewayQuery>();
      Mutation = provider.GetRequiredService<LookForGatewayMutation>();
    }
  }
}
