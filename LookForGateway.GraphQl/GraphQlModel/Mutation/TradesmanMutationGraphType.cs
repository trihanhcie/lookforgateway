﻿using GraphQL;
using GraphQL.Types;
using Grpc.Core;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.GraphQlModel.Types.Mutation;
using LookForGateway.GraphQl.Model;
using LookForGateway.UseCases.Tradesman.Commands;
using MediatR;

namespace LookForGateway.GraphQl.GraphQlModel.Mutation
{
  public class TradesmanMutationGraphType : ObjectGraphType
  {
    public TradesmanMutationGraphType(IMediator mediaTr)
    {
      Name = GraphQLArgumentConstant.TradesmanMutationGroup;
      FieldAsync<UpdateTradesmanMutationResponseType>(GraphQLTypeConstant.Tradesman.UpdateByEmail,
         arguments: new QueryArguments(new QueryArgument<NonNullGraphType<UpdateTradesmanMutationType>> { Name = GraphQLArgumentConstant.Input }),
               resolve: async context =>
               {
                 try
                 {
                   var command = context.GetArgument<UpdateTradesmanCommand>(GraphQLArgumentConstant.Input);
                   await mediaTr.Send(command);

                   return new UpdateTradesmanResponseModel
                   {
                     Email = command.Email
                   };
                 }
                 catch(RpcException rpcExp)
                 {
                   throw new ExecutionError(rpcExp.Message, rpcExp);
                 }
               });
    }
  }
}
