﻿using GraphQL;
using GraphQL.Types;
using Grpc.Core;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.GraphQl.Model;
using LookForGateway.UseCases.Jobs.Commands;
using MediatR;
using System.Linq;

namespace LookForGateway.GraphQl.GraphQlModel.Mutation
{
  public class JobMutationGraphType : ObjectGraphType
  {
    public JobMutationGraphType(IMediator mediaTr)
    {
      Name = GraphQLArgumentConstant.JobMutationGroup;
      FieldAsync<AddJobLeadMutationResponseType>(GraphQLTypeConstant.Job.AddLeadByEmail,
                 arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AddJobLeadMutationType>> { Name = GraphQLArgumentConstant.Input }),
                       resolve: async context =>
                       {
                         try
                         {
                           var command = context.GetArgument<AddJobLeadByTradesmanEmailCommand>(GraphQLArgumentConstant.Input);
                           await mediaTr.Send(command);

                           return new AddJobLeadResponseModel
                           {
                             Code = command.Code
                           };
                         }
                         catch (RpcException rpcExp) when (rpcExp.Status.StatusCode == StatusCode.InvalidArgument)
                         {
                           throw new ExecutionError(rpcExp.Status.Detail, rpcExp.Trailers.ToDictionary(x => x.Key, x => x.Value));
                         }
                       });
    }
  }
}
