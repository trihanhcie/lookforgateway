﻿using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.UseCases.Jobs.Commands;

namespace LookForGateway.GraphQl.GraphQlModel.Mutation
{
  public class AddJobLeadMutationType : InputObjectGraphType<AddJobLeadByTradesmanEmailCommand>
  {
    public AddJobLeadMutationType()
    {
      Name = nameof(AddJobLeadMutationType);
      Field<NonNullGraphType<StringGraphType>>(GraphQLArgumentConstant.Email);
      Field<StringGraphType>(GraphQLArgumentConstant.Job.Code);
      Field<StringGraphType>(GraphQLArgumentConstant.Job.Description);
      Field<StringGraphType>(GraphQLArgumentConstant.Job.FullAddress);

    }
  }
}