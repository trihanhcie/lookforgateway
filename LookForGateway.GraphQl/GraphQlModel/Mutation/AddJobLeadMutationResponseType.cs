﻿using GraphQL.Types;
using LookForGateway.GraphQl.Model;

namespace LookForGateway.GraphQl.GraphQlModel.Mutation
{
  public class AddJobLeadMutationResponseType : ObjectGraphType<AddJobLeadResponseModel>
  {
    public AddJobLeadMutationResponseType()
    {
      Field(x => x.Succeeded);
      Field(x => x.RequestedAt);
      Field(x => x.Code);
    }
  }
}
