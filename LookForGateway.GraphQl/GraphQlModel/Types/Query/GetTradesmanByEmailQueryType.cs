﻿using GraphQL.Types;
using LookForGateway.UseCases.Tradesman.Queries;

namespace LookForGateway.GraphQl.GraphQlModel.Types.Query
{
  public class GetTradesmanByEmailQueryType : ObjectGraphType<GetTradesmanByEmailQueryResult>
  {
    public GetTradesmanByEmailQueryType()
    {
      Field(x => x.FirstName);
      Field(x => x.LastName);
    }
  }
}
