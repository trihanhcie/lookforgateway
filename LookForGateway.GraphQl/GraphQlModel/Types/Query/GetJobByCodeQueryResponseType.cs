﻿using GraphQL.Types;
using LookForGateway.UseCases.Jobs.Queries;

namespace LookForGateway.GraphQl.GraphQlModel.Types.Query
{
  public class GetJobByCodeQueryResponseType : ObjectGraphType<GetJobByCodeQueryResult>
  {
    public GetJobByCodeQueryResponseType()
    {
      Field(x => x.Code);
      Field(x => x.Description);
    }
  }
}
