﻿using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.UseCases.Jobs.Queries;

namespace LookForGateway.GraphQl.GraphQlModel.Types.Query
{
  public class GetJobByCodeQueryType : InputObjectGraphType<GetJobByCodeQuery>
  {
    public GetJobByCodeQueryType()
    {
      Name = nameof(GetJobByCodeQueryType);
      Field<NonNullGraphType<StringGraphType>>(GraphQLArgumentConstant.Job.Code);
      Field<NonNullGraphType<StringGraphType>>(GraphQLArgumentConstant.Email);
    }
  }
}
