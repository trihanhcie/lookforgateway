﻿using GraphQL.Types;
using LookForGateway.GraphQl.GraphQlModel.Constants;
using LookForGateway.UseCases.Tradesman.Commands;

namespace LookForGateway.GraphQl.GraphQlModel.Types.Mutation
{
  public class UpdateTradesmanMutationType : InputObjectGraphType<UpdateTradesmanCommand>
  {
    public UpdateTradesmanMutationType()
    {
      Name = nameof(UpdateTradesmanMutationType);
      Field<NonNullGraphType<StringGraphType>>(GraphQLArgumentConstant.Email);
      Field<StringGraphType>(GraphQLArgumentConstant.Tradesman.FirstName);
      Field<StringGraphType>(GraphQLArgumentConstant.Tradesman.LastName);
      Field<StringGraphType>(GraphQLArgumentConstant.Tradesman.FullAddress);
    }
  }
}
