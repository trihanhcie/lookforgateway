﻿using GraphQL.Types;
using LookForGateway.GraphQl.Model;

namespace LookForGateway.GraphQl.GraphQlModel.Types.Mutation
{
  public class UpdateTradesmanMutationResponseType : ObjectGraphType<UpdateTradesmanResponseModel>
  {
    public UpdateTradesmanMutationResponseType()
    {
      Field(x => x.Succeeded);
      Field(x => x.RequestedAt);
      Field(x => x.Email);
    }
  }
}
