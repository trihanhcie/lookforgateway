﻿using System;

namespace LookForGateway.GraphQl.Model
{
  public abstract class BaseMutationResponseModel
  {
    public bool Succeeded { get; protected set; } = true;
    public DateTimeOffset RequestedAt { get; protected set; } = DateTimeOffset.Now;
  }
}
