﻿namespace LookForGateway.GraphQl.Model
{
  public class UpdateTradesmanResponseModel : BaseMutationResponseModel
  {
    public string Email { get; set; }
  }
}
