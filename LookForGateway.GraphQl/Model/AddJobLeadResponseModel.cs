﻿namespace LookForGateway.GraphQl.Model
{
  public class AddJobLeadResponseModel : BaseMutationResponseModel
  {
    public string Code { get; set; }
  }
}
