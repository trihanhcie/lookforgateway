using Autofac;
using GraphQL.Server;
using GraphQL.Server.Ui.GraphiQL;
using LookForDevPackages.EventBus.DI;
using LookForGateway.Core.Config;
using LookForGateway.GraphQl.DI;
using LookForGateway.GraphQl.GraphQlModel;
using LookForGateway.GraphQl.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shared.gRPC;
using System;

namespace LookForGateway.GraphQl
{
  public class Startup
  {
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().AddNewtonsoftJson(options =>
   options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

      var appSettings = new ApplicationSettingConfig();
      Configuration.Bind(appSettings);
      services.AddGrpcClient<TradesmanService.TradesmanServiceClient>(o =>
      {
        o.Address = new Uri(appSettings.RPCConfig.LookToManageRPCEndpoint);
      });
      services.AddGrpcClient<JobService.JobServiceClient>(o =>
         {
           o.Address = new Uri(appSettings.RPCConfig.LookToManageRPCEndpoint);
         });

      services.AddGraphQL(options =>
      {
        options.EnableMetrics = true;
      })
        .AddSystemTextJson(deserializerSettings => { }, serializerSettings => { });

      services.Configure<KestrelServerOptions>(options =>
      {
        options.AllowSynchronousIO = true;
      });

      // IIS
      services.Configure<IISServerOptions>(options =>
      {
        options.AllowSynchronousIO = true;
      });
    }

    public void ConfigureContainer(ContainerBuilder builder)
    {
      builder.RegisterModule(new GraphQLModule());
      builder.RegisterModule(new MapperModule());
      builder.RegisterModule(new EventBusModule());
      builder.RegisterModule(new MediaTrModule());

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
      }
      app.UseRouting();
      app.UseExceptionHandlingMiddleware();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });

      app.UseGraphQL<LookForGatewaySchema>("/graphql");

      app.UseGraphiQLServer(new GraphiQLOptions());

    }
  }
}
