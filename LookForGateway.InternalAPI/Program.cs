﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace LookForGateway.InternalAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
             .ConfigureWebHostDefaults(webHostBuilder =>
             {
                 webHostBuilder
                 .UseContentRoot(Directory.GetCurrentDirectory())
                 .UseIISIntegration()
                 .UseStartup<Startup>();
             })
             .Build();

            host.Run();
        }
    }
}