﻿using MediatR;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace LookForGateway.Common.Extensions.MediaTR
{
  public class LoggingBehavior<TRequest, TResponse> :
      IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
  {
    private readonly ILogger _logger;

    public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger)
    {
      _logger = logger;
    }

    public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
    {
      var timer = new Stopwatch();
      _logger.LogInformation("Start processing {requestName} {@request}", request.GetType(), request);
      timer.Start();
      var response = await next();
      timer.Stop();
      _logger.LogInformation("Request {requestName} processed in {timer}", request.GetType(), timer.ElapsedMilliseconds);

      return response;
    }
  }
}
