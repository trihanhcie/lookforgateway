﻿using Grpc.Core;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LookForGateway.Common.Extensions.MediaTR
{
  public class ExceptionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
  {
    private readonly ILogger _logger;
    public ExceptionBehavior(ILogger<ExceptionBehavior<TRequest, TResponse>> logger)
    {
      _logger = logger;
    }
    public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
    {
      try
      {
        var response = await next();
        return response;
      }
      catch (RpcException rpcExp)
      {
        _logger.LogError(rpcExp, "RPC Exception {@request}", request.ToString());
        throw;
      }
      catch (Exception exp)
      {
        _logger.LogError(exp, "Exception {@request}", request.ToString());
        throw;
      }
    }
  }
}
