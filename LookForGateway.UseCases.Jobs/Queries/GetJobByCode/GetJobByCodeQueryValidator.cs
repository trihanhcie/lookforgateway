﻿using FluentValidation;

namespace LookForGateway.UseCases.Jobs.Queries
{
  public class GetJobByCodeQueryValidator : AbstractValidator<GetJobByCodeQuery>
  {
    public GetJobByCodeQueryValidator()
    {
      RuleFor(job => job.Code).MinimumLength(10);
      RuleFor(job => job.Email).NotEmpty().EmailAddress();
    }
  }
}
