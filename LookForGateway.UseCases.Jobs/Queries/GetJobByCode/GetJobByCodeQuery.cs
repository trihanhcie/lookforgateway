﻿using AutoMapper;
using MediatR;
using Shared.gRPC;
using System.Threading;
using System.Threading.Tasks;

namespace LookForGateway.UseCases.Jobs.Queries
{
  public class GetJobByCodeQuery : IRequest<GetJobByCodeQueryResult>
  {
    public string Code { get; set; }
    public string Email { get; set; }
  }

  public class GetJobByCodeQueryHandler : IRequestHandler<GetJobByCodeQuery, GetJobByCodeQueryResult>
  {
    private readonly JobService.JobServiceClient _client;
    private readonly IMapper _mapper;
    
    public GetJobByCodeQueryHandler(JobService.JobServiceClient client, IMapper mapper)
    {
      _client = client;
      _mapper = mapper;
    }

    public async Task<GetJobByCodeQueryResult> Handle(GetJobByCodeQuery request, CancellationToken cancellationToken)
    {
      var response = await _client.GetByCodeAsync(new GetJobByCodeRequest
      {
        Code = request.Code,
        Email = request.Email
      });

      return _mapper.Map<GetJobByCodeResponse, GetJobByCodeQueryResult>(response);
    }
  }
}
