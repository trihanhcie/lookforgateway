﻿namespace LookForGateway.UseCases.Jobs.Queries
{
  public class GetJobByCodeQueryResult
  {
    public string Code { get; set; }
    public string Description { get; set; }
  }
}
