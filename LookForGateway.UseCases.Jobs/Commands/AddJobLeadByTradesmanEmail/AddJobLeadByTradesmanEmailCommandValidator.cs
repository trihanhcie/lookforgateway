﻿using FluentValidation;

namespace LookForGateway.UseCases.Jobs.Commands
{
  public class AddJobLeadByTradesmanEmailCommandValidator : AbstractValidator<AddJobLeadByTradesmanEmailCommand>
  {
    public AddJobLeadByTradesmanEmailCommandValidator()
    {
      RuleFor(job => job.Email).NotNull().EmailAddress();
      RuleFor(job => job.Description).NotEmpty();
      RuleFor(job => job.Code).MinimumLength(10);
    }
  }
}