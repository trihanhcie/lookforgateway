﻿using AutoMapper;
using MediatR;
using Shared.gRPC;
using System.Threading;
using System.Threading.Tasks;


namespace LookForGateway.UseCases.Jobs.Commands
{
  public class AddJobLeadByTradesmanEmailCommand : IRequest
  {
    public string Email { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }
    public string FullAddress { get; set; }
  }

  public class AddJobLeadByTradesmanEmailCommandHandler : IRequestHandler<AddJobLeadByTradesmanEmailCommand>
  {
    private readonly IMapper _mapper;
    private readonly JobService.JobServiceClient _client;
    
    public AddJobLeadByTradesmanEmailCommandHandler(IMapper mapper, JobService.JobServiceClient client)
    {
      _mapper = mapper;
      _client = client;
    }

    public async Task<Unit> Handle(AddJobLeadByTradesmanEmailCommand command, CancellationToken cancellationToken)
    {
      var request = _mapper.Map<AddJobLeadByTradesmanEmailCommand, AddJobLeadByTradesmanEmailRequest>(command);
      await _client.AddLeadByTradesmanEmailAsync(request);
      return Unit.Value;
    }
  }
}
