﻿using AutoMapper;
using LookForGateway.UseCases.Jobs.Commands;
using LookForGateway.UseCases.Jobs.Queries;
using Shared.gRPC;

namespace LookForGateway.UseCases.Jobs.Mapper
{
  public class JobProfile : Profile
  {
    public JobProfile()
    {
      CreateMap<AddJobLeadByTradesmanEmailCommand, AddJobLeadByTradesmanEmailRequest>();
      CreateMap<GetJobByCodeResponse, GetJobByCodeQueryResult>();

    }
  }
}
