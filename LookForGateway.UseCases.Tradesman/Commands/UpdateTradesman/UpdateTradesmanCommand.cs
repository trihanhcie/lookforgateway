﻿using AutoMapper;
using MediatR;
using Shared.gRPC;
using System.Threading;
using System.Threading.Tasks;

namespace LookForGateway.UseCases.Tradesman.Commands
{
  public class UpdateTradesmanCommand : IRequest
  {
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string FullAddress { get; set; }
  }

  public class UpdateTradesmanCommandHandler : IRequestHandler<UpdateTradesmanCommand>
  {
    private readonly IMapper _mapper;
    private readonly TradesmanService.TradesmanServiceClient _client;

    public UpdateTradesmanCommandHandler(TradesmanService.TradesmanServiceClient client, IMapper mapper)
    {
      _client = client;
      _mapper = mapper;
    }

    public async Task<Unit> Handle(UpdateTradesmanCommand command, CancellationToken cancellationToken)
    {
      var request = _mapper.Map<UpdateTradesmanCommand, UpdateTradesmanRequest>(command);
      await _client.UpdateAsync(request);
      return Unit.Value;
    }
  }
}
