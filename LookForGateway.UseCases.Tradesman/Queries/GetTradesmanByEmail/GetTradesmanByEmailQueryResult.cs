﻿namespace LookForGateway.UseCases.Tradesman.Queries
{
  public class GetTradesmanByEmailQueryResult
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
  }
}
