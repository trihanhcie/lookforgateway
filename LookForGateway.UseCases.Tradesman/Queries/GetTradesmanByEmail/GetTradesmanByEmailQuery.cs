﻿using AutoMapper;
using MediatR;
using Shared.gRPC;
using System.Threading;
using System.Threading.Tasks;


namespace LookForGateway.UseCases.Tradesman.Queries
{
  public class GetTradesmanByEmailQuery : IRequest<GetTradesmanByEmailQueryResult>
  {
    public string Email { get; set; }
  }

  public class GetTradesmanByEmailQueryHandler : IRequestHandler<GetTradesmanByEmailQuery, GetTradesmanByEmailQueryResult>
  {
    private readonly TradesmanService.TradesmanServiceClient _client;
    private readonly IMapper _mapper;

    public GetTradesmanByEmailQueryHandler(TradesmanService.TradesmanServiceClient client, IMapper mapper)
    {
      _client = client;
      _mapper = mapper;
    }

    public async Task<GetTradesmanByEmailQueryResult> Handle(GetTradesmanByEmailQuery request, CancellationToken cancellationToken)
    {
      var tradesmanResponse = await _client.GetByEmailAsync(new GetTradesmanByEmailRequest
      {
        Email = request.Email
      });

      return _mapper.Map<GetTradesmanByEmailResponse, GetTradesmanByEmailQueryResult>(tradesmanResponse);
    }
  }
}
