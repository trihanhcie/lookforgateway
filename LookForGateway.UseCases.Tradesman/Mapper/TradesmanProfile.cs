﻿using AutoMapper;
using LookForGateway.UseCases.Tradesman.Commands;
using Shared.gRPC;

namespace LookForGateway.UseCases.Tradesman.Mapper
{
  public class TradesmanProfile : Profile
  {
    public TradesmanProfile()
    {
      CreateMap<UpdateTradesmanCommand, UpdateTradesmanRequest>();
    }
  }
}
